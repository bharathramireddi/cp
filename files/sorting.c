#include<stdio.h>
void merge(int *a, int s, int e) {
  int mid = (s + e) / 2;

  int i = s;
  int j = mid + 1;
  int k = s;

  int temp[100];

  while (i <= mid && j <= e) {
    if (a[i] < a[j]) {
      temp[k++] = a[i++];
    } else {
      temp[k++] = a[j++];
    }
  }
  while (i <= mid) {
    temp[k++] = a[i++];
  }
  while (j <= e) {
    temp[k++] = a[j++];
  }

  for (int i = s; i <= e; i++) {
    a[i] = temp[i];
  }
}

void mergeSort(int a[], int s, int e) {
  if (s >= e) {
    return;
  }

  int mid = (s + e) / 2;
  mergeSort(a, s, mid);
  mergeSort(a, mid + 1, e);
  merge(a, s, e);
}

int main()
{
    FILE *f1;
    f1=fopen("record.txt", "r");
    if(f1==NULL)
    {
        printf("Erro\n");
        return 0;
    }    
    int len,numbers[100], num, i=0;
    while(!feof(f1))
    {   fscanf(f1,"%d",&num);
        numbers[i]=num;
        i++;    
    }
    fclose(f1);
    len=strlen(numbers);
    mergeSort(numbers,0,i-1);
    printf("\n   Sorted Order \n");
    for(i=0;i<10;i++){
      printf(" %d ",numbers[i]);
    }
    printf("\n");
}


