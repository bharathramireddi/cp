#include <stdio.h>
int main() {
  char a[100];
  int count = 0, i, len, space = 0, con = 0;
  FILE *f1;
  f1 = fopen("text.txt", "r");
  while (!feof(f1)) {
    fgets(a, 100, f1);
  }
  fclose(f1);
  len = strlen(a);
  for (i = 0; i < len; i++) {
    if (a[i] == 'a' || a[i] == 'e' || a[i] == 'i' || a[i] == 'o' ||
        a[i] == 'u') {
      count++;
    } else if (a[i] == ' ') {
      space++;
    } else if ((a[i] >= 'a' && a[i] <= 'z')) {
      con++;
    }
  }
  printf("\nVowels-->%d\nSpace--%d\nConsonents-->%d\n", count, space,
         con);

  return 0;
}
