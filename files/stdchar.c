#include <stdio.h>
int main() {
  char n, a[32];
  int i, j = 0, k;
  FILE *f1;
  f1 = fopen("binchar.txt", "wb");
  printf("enter the n value :");
  scanf("%c", &n);
  for (i = 31; i >= 0; i--) {
    k = n >> i;
    if (k & 1) {
      a[j] = 1;
      j++;
    } else {
      a[j] = 0;
      j++;
    }
  }
  for (i = 0; i <= 31; i++) {
    fprintf(f1, "%d", a[i]);
  }
  fclose(f1);
}
