#include <stdio.h>
#include <string.h>

struct student {
  int id;
  char name[20];
  int percent;
};

int main() {
  int i = 0, n;
  FILE *f1;
  f1 = fopen("Structtext.txt", "wb");
  struct student record[10];
  printf("Enter no of students ");
  scanf("%d", &n);
  while (i < n) {
    printf("Enter id  ");
    scanf("%d", &record[i].id);
    printf("Enter Percent ");
    scanf("%d", &record[i].percent);
    printf("Enter name  ");
    scanf("%s", &record[i].name);
    i++;
  }
  i = 0;
  while (i < n) {
    fprintf(f1, " Id : %d \n", record[i].id);
    fprintf(f1, " Name : %s \n", record[i].name);
    fprintf(f1, " Percentage : %d \n", record[i].percent);
    i++;
  }
  fclose(f1);
}
