#include <stdio.h>
#include <stdlib.h>
int main() {
  char var[100], input[100];
  FILE *f1;
  f1 = fopen("text.txt", "w");
  if (f1 == NULL) {
    printf("Error");
    exit(1);
  } else {
    printf("Enter Sentence : ");
    scanf("%s", &input);
    fprintf(f1, "%s", input);
    fclose(f1);
  }
  return 0;
}
