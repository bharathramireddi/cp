#include <stdio.h>
int main() {
  int a;
  float b;
  char c;
  long int *adr1, *adr2, num1, num2, p1, p2;
  num1 = 10;
  num2 = 20;
  adr1 = &num1;
  adr2 = &num2;
  printf("Values %d---%d\n", num1, num2);
  printf("Address of Values-- %p---%p\n", adr1, adr2);
  printf("After indirection using pointers\n");
  *adr1 = 100;
  *adr2 = 200;
  printf("Values %d %d \n", num1, num2);
  printf("Address are same %p %p \n ", adr1, adr2);
  printf("\n %d--->int\n %d-->float \n %d-->char\n", &a, &b, &c);
}
