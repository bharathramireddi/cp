#include <stdio.h>
int largesmall(int ar[10], int *max, int *min, int j) {
  *min = *max = ar[0];
  for (j = 0; j < 5; j++) {
    if (ar[j] > *max) {
      *max = ar[j];
    } else if (ar[j] < *min) {
      *min = ar[j];
    }
  }
}
int main() {
  int arr[10], i, big, small;
  printf("Enter integers");
  for (i = 0; i < 5; i++) {
    scanf("%d", &arr[i]);
  }
  largesmall(arr, &big, &small, 0);
  printf("\nlarge-->%d\nsmall->%d\n", big, small);
}
