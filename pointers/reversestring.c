#include <stdio.h>
int main() {
  int len, i;
  char s[10];
  char *ptr;
  printf("Enter string : ");
  scanf("%s", &s);
  len = strlen(s);
  ptr = &s;
  for (i = len; i >= 0; i--) {
    printf("%c", *(ptr + i));
  }
}
