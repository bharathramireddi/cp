#include <stdio.h>
int fact(int *num);
int main() {
  int n = 6;
  int res;
  res = fact(&n);
  printf("\n%d", res);
}

int fact(int *num) {
  int i, facto = 1;
  for (i = 1; i <= *num; i++) {
    facto = facto * i;
  }
  return facto;
}
