#include <stdio.h>
int main() {
  double a;
  float b;
  int c, av, bv, cv, dv, ev;
  char d;
  long e;
  av = sizeof(a);
  bv = sizeof(b);
  cv = sizeof(c);
  dv = sizeof(d);
  ev = sizeof(e);
  printf("%d %d %d %d %d \n", av, bv, cv, dv, ev);
  av = sizeof(double *);
  bv = sizeof(float *);
  cv = sizeof(int *);
  dv = sizeof(char *);
  ev = sizeof(long *);
  printf("%d %d %d %d %d\n", av, bv, cv, dv, ev);
}
