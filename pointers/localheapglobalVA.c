#include <stdio.h>
int global = 10;
int main() {
  int local;
  int heap;
  printf("'%d", getpid());
  printf("Enter Local=");
  scanf("%d", &local);
  printf("%d", getpid());
  printf("\nEnter Heap=");
  heap = malloc(sizeof(int));
  scanf("%d", &heap);
  printf("local-->%p\n", &local);
  printf("global-->%p\n", &global);
  printf("Heap-->%p\n", &heap);
}
