#include <stdio.h>
int add(int, int);
int main(int argc, char *argv[]) {
  int a = 3, b = 4, sum = 0;
  a = atoi(argv[1]);
  b = atoi(argv[2]);
  int (*p)(int, int);
  p = add;
  sum = p(a, b);
  printf("Addition is %d", sum);
}
int add(int a, int b) { return a + b; }
