#include <stdio.h>
int display(int *arr[], int num) {
  int i;
  for (i = 0; i < num; i++) {
    printf("\n %d", *arr[i]);
  }
}
int main() {
  int array[20] = {1, 2, 3, 4};
  int *parray[20] = {array + 0, array + 1, array + 2, array + 3};
  display(parray, 4);
}
