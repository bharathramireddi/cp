#include <stdio.h>
int *ag;
float *bg;
char *cg;
double *dg;
void main() {
  float *bl, *bh;
  int *al, *ah;
  char *cl, *ch;
  double *dl, *dh;
  ah = malloc(sizeof(int));
  bh = malloc(sizeof(float));
  ch = malloc(sizeof(char));
  dh = malloc(sizeof(double));
  printf("int     global-->%p  local-->%p heap-->%p\n", ag, al, ah);
  printf("Float   global-->%p  local-->%p heap-->%p\n", bg, bl, bh);
  printf("Char    global-->%p  local-->%p heap-->%p\n", cg, cl, ch);
  printf("Double  global-->%p  local-->%p heap-->%p\n", dg, dl, dh);
}
