#include <stdio.h>
int main() {
  int i, j, k, a[10][10], b[10][10], c[10][10], row = 2, col = 2, ch;

  for (i = 0; i < row; i++) {
    for (j = 0; j < col; j++) {
      printf("A %d%d ", i + 1, j + 1);
      scanf("%d", &a[i][j]);
    }
  }
  for (i = 0; i < row; i++) {
    for (j = 0; j < col; j++) {
      printf("B %d%d ", i + 1, j + 1);
      scanf("%d", &b[i][j]);
    }
  }
  printf("Menu :\n");
  printf("1.Add\n");
  printf("2.Subtract\n");
  printf("3.Multiply\n");
  scanf("%d", &ch);
  switch (ch) {
  case 1:
    for (i = 0; i < row; i++) {
      for (j = 0; j < col; j++) {
        c[i][j] = *(*(a + i) + j) + *(*(b + i) + j);
      }
    };
    break;
  case 2:
    for (i = 0; i < row; i++) {
      for (j = 0; j < col; j++) {
        c[i][j] = *(*(a + i) + j) - *(*(b + i) + j);
      }
    };
    break;
  case 3:
    for (i = 0; i < row; i++) {
      for (j = 0; j < col; j++) {
        c[i][j] = 0;
      }
    }
    for (i = 0; i < row; i++) {
      for (j = 0; j < col; j++) {
        for (k = 0; k < col; k++) {
          c[i][j] = c[i][j] + (*(*(a + i) + k)) * (*(*(b + k) + j));
        }
      }
    };
    break;
  }
  for (i = 0; i < row; i++) {
    for (j = 0; j < col; j++) {
      printf("%d \t", *(*(c + i) + j));
    }
    printf("\n");
  }
}
