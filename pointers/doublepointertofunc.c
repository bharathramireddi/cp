#include <stdio.h>
int Display(int **ptr) { 
  *(*ptr) = *(*ptr) + 10;
 }
int main() {
  int a = 2, *ptr1;
  printf("\nA value= %d", a);
  ptr1 = &a;
  Display(&ptr1);
  printf("\nAfter function call");
  printf("\nA value --> %d\n", a);
}
