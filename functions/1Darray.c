#include <stdio.h>
float average(int n, int arr[10]);
int main() {
  int num = 7, arr[10] = {12, 20, 35, 60, 34, 20, 10};
  float avg;
  avg = average(num, arr);
  printf("\n Average : %f\n", avg);
}
float average(int n, int arr[10]) {
  int i;
  float sum = 0, avg;
  for (i = 0; i < n; i++) {
    sum += arr[i];
  }
  avg = sum / n;
  return avg;
}
