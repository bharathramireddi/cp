#include <stdio.h>
int rowsum(int arr[3][3]);
int main(int argc, char *argv[]) {
  int arr[3][3] = {1, 2, 3, 4, 5, 6, 7, 8, 9}, sum = 0;
  rowsum(arr);
}
int rowsum(int arr[3][3]) {
  int i, j, sum = 0;
  for (i = 0; i < 3; i++) {
    for (j = 0; j < 3; j++) {
      sum += arr[i][j];
    }
    printf("\nSum of row-%d is %d\n", i + 1, sum);
    sum = 0;
  }
  return 0;
}
