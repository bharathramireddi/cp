#include <math.h>
#include <stdio.h>
int mean(int a[10], int num);
int standarddev(int a[10], float average, int num);
int main() {
  int x[10];
  int i, n;
  float avg;
  printf("Enter the value of N \n");
  scanf("%d", &n);
  printf("Enter %d real numbers \n", n);
  for (i = 0; i < n; i++) {
    scanf("%d", &x[i]);
  }
  avg = mean(x, n);
  standarddev(x, avg, n);
}
int mean(int a[10], int num) {
  int i;
  float sum = 0, avg;
  for (i = 0; i < num; i++) {
    sum = sum + a[i];
  }
  avg = sum / num;
  printf("mean -- %f\n", avg);
}
int standarddev(int a[10], float average, float num) {
  float sum1 = 0, standard, variance;
  int i;
  for (i = 0; i < num; i++) {
    sum1 = sum1 + pow((a[i] - average), 2);
  }
  variance = sum1 / float(num);
  standard = sqrt(variance);
  printf("variance  -- %f\n", variance);
  printf("Standard deviation -- %f\n", standard);
}
