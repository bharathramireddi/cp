#include <stdio.h>
#include <string.h>

#define N 5

void Display(char **temp, int num);

void main() {
	char *str[N] = {"Bharath", "Deepak", "AUDI", "Bentley"};
	Display(str, N);
}

void Display(char **temp, int n) {
	  int i;
  	printf("\nStrings passed to this function are\n");
  	for (i = 0; i < n; i++) {
    	printf("\t");
    	printf(temp[i]);
  	}
}
