#include <stdio.h>
struct vehicle {
  char name[20];
  int value;
} c;
void display(struct vehicle cars) {
  printf("\nName : %s\n", cars.name);
  printf("\nValue : %d\n", cars.value);
}
void main() {
  printf("\nEnter Car Name :");
  scanf("%s", &c.name);
  printf("Enter its Value : ");
  scanf("%d", &c.value);
  display(c);
}
