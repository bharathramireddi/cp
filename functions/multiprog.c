#include <stdio.h>
int fact(int a);
int main() {
  int ch, num, r, npr, ncr;
  printf("Enter n value ");
  scanf("%d", &num);
  printf("Enter r value ");
  scanf("%d", &r);
  printf("Menu \n 1.nCr\n 2.nPr\n");
  npr = fact(num) / fact(num - r);
  ncr = fact(num) / (fact(num - r) * fact(r));
  scanf("%d", &ch);
  switch (ch) {
  case 1:
    printf("nCr--> %d", ncr);
    break;
  case 2:
    printf("nPr-->%d", npr);
    break;
  }
}
int fact(int a) {
  int i, facto = 1;
  for (i = 1; i <= a; i++) {
    facto = facto * i;
  }
  return facto;
}
