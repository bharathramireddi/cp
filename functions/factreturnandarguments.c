#include <stdio.h>
int fact(int a);
int main() {
  int num, factorial;
  printf("Enter number");
  scanf("%d", &num);
  factorial = fact(num);
  printf("Factorial -- %d ", factorial);
}
int fact(int a) {
  int i, facto = 1;
  for (i = 1; i <= a; i++) {
    facto = facto * i;
  }
  return facto;
}
