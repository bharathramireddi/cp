#include <stdio.h>
int fact(int a);
int main() {
  int num, factorial, ncr, r;
  printf("Enter n value ");
  scanf("%d", &num);
  printf("Enter r value ");
  scanf("%d", &r);
  ncr = fact(num) / (fact(num - r) * fact(r));

  printf("\n n-C-r -- %d ", ncr);
}
int fact(int a) {
  int i, facto = 1;
  for (i = 1; i <= a; i++) {
    facto = facto * i;
  }
  return facto;
}
