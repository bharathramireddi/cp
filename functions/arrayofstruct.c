#include <stdio.h>
struct car {
  char name[20];
  int year;
};
void accept(struct car cars[], int num) {
  int i;
  for (i = 0; i < num; i++) {
    printf("\nCar Name : ");
    scanf("%s", &cars[i].name);
    printf("\nCar Year : ");
    scanf("%d", &cars[i].year);
  }
}
void main() {
  int i;
  struct car c[3];
  accept(c, 3);
  for (i = 0; i < 3; i++) {
    printf("\nCar Name : %s ", c[i].name);
    printf("\nCar Year : %d ", c[i].year);
  }
}
