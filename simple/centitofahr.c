#include <stdio.h>

int main() {
  float centigrade, fahrenheit;
  printf("Enter temperature in centigrade : ");
  scanf("%f", &centigrade);
  fahrenheit = (1.8 * centigrade) + 32;
  printf("Result : %f F\n", fahrenheit);
  return 0;
}
