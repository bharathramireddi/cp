
#include <stdio.h>
int main() {
  float degrees, radians, pi = 3.14;
  printf("Enter Radians : ");
  scanf("%f", &radians);
  degrees = radians * (180 / pi);
  printf("In Degrees : %f \n", degrees);
  return 0;
}
