#include <stdio.h>

int main() {
  float breadth, height, area;
  printf("Enter Breadth : ");
  scanf("%f", &breadth);
  printf("Enter Height : ");
  scanf("%f", &height);
  area = (breadth * height) / 2;
  printf("Result : %f \n", area);
  return 0;
}
