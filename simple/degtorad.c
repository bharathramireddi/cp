#include <stdio.h>
int main() {
  float degrees, radians, pi = 3.14;
  printf("Enter Degrees : ");
  scanf("%f", &degrees);
  radians = degrees * (pi / 180);
  printf("In Radians : %f \n", radians);
  return 0;
}
