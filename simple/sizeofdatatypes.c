#include <stdio.h>

int main() {
  int a, b, c, d, e;
  a = sizeof(int);
  b = sizeof(float);
  c = sizeof(char);
  d = sizeof(long);
  e = sizeof(double);
  printf("Size of Datatypes\n");
  printf("int-%d\n", a);
  printf("float-%d\n", b);
  printf("char-%d\n", c);
  printf("long-%d\n", d);
  printf("double-%d\n", e);
  return 0;
}
