#include <stdio.h>
int main() {
  int marks;
  printf("Enter your marks ");
  scanf("%d", &marks);
  if (marks >= 80 && marks <= 100) {
    printf("Your Grade : A\n");
  }
  if (marks >= 60 && marks < 80) {
    printf("Your Grade : B\n");
  }
  if (marks >= 40 && marks < 60) {
    printf("Your Grade : C\n");
  }
  if (marks < 40) {
    printf("You failed\n");
  }
}
