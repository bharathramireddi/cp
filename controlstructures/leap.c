#include <stdio.h>

int main() {
  int year, leap;
  printf("Enter the Year to Find Leap or Not : ");
  scanf("%d", &year);
  leap = year % 4;
  if (leap == 0) {
    printf(" %d : Leap year\n ", year);
  } else {
    printf(" %d : Not a Leap Year\n", year);
  }
  return 0;
}
