#include <stdio.h>
int main() {
  int num, temp, rem, rev = 0;
  printf("Enter number \n");
  scanf("%d", &num);
  while (num > 0) {
    rem = num % 10;
    rev = rev * 10 + rem;
    num = num / 10;
  }
  printf("after reverse is  = %d\n", rev);
}
