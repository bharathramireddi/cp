#include <stdio.h>

int main ()
{ 
  int num, even;
  printf ("Enter the Number : ");
  scanf ("%d", &num);
  even = num % 2;
  if (even == 0)
   {
       printf ("Number is %d : Even\n ", num);
    }
  else
   {
       printf ("Number is %d : Odd\n", num);
   }
  return 0;
}
