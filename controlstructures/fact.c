#include <stdio.h>
int main() {
  unsigned n, i;
  long double factorial = 1;

  printf("Enter a number: ");
  scanf("%u", &n);
  if (n < 0) {
    printf("error input\n");
  }
  for (i = 2; i <= n; i++) {
    factorial *= i;
  }
  printf("Factorial of %u = %Lf\n", n, factorial);
}
