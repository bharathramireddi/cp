#include <stdio.h>
int bc(int n, int r);

int main() {
  int n, r;
  printf("Enter n and k : ");
  scanf("%d%d", &n, &r);
  printf("\nBinomial coefficient\n", bc(n, r));
  printf("%d\n", bc(n, r));

  return 0;
}

int bc(int n, int r) {
  if (r == 0 || r == n)
    return 1;
  return bc(n - 1, r - 1) + bc(n - 1, r);
}
