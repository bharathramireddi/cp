#include <stdio.h>
int fact(int num);
int main() {
  int res, num;
  printf("Enter Number ");
  scanf("%d", &num);
  res = fact(num);
  printf("Factorial--> %d\n", res);
}

int fact(int n) {
  if (n >= 1) {
    return n * fact(n - 1);
  } else {
    return 1;
  }
}
