#include <stdio.h>
int main() {
  int a[20], i, j, n, key;
  printf("Enter no of  elements ");
  scanf("%d", &n);
  printf("Enter %d Element\n", n);
  for (i = 0; i < n; i++) {
    scanf("%d", &a[i]);
  }
  bubblesort(a, n);
  printf("Sorted order \n ");
  for (i = 0; i < n; i++) {
    printf(" %d ", a[i]);
  }
  printf("\nEnter key to search ");
  scanf("%d", &key);
  binarysearch(key, n, 0, a);
}
void bubblesort(int a[], int n) {
  int temp, i, j;
  for (i = 0; i < n; i++) {
    for (j = i; j < n; j++) {
      if (a[i] > a[j]) {
        temp = a[i];
        a[i] = a[j];
        a[j] = temp;
      }
    }
  }
}
void binarysearch(int key, int high, int low, int a[]) {
  int mid;
  if (low > high) {
    printf("Key Not Found\n");
    return;
  }
  mid = (low + high) / 2;
  if (a[mid] == key) {
    printf("Key Found\n");
    return;
  } else if (a[mid] > key) {
    return binarysearch(key, mid - 1, low, a);
  } else if (a[mid] < key) {
    return binarysearch(key, high, mid + 1, a);
  }
  return 0;
}
