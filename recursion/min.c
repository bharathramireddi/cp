#include <stdio.h>
#define MAX 10
int getMinElement(int[]); // takes array of int as parameter
int size;
int main() {
  printf("\n\n\t\tStudytonight - Best place to learn\n\n\n");
  int arr[MAX], min, i;
  printf("\n\nEnter the size of the array: ");
  scanf("%d", &size);
  printf("\n\nEnter %d elements\n\n", size);
  for (i = 0; i < size; i++) {
    scanf("%d", &arr[i]);
  }
  min = getMinElement(arr);
  printf("\n\nSmallest element of the array is %d\n\n", min);
  return 0;
}

int getMinElement(int a[]) {
  int i = 0, min = 9999;
  if (i < size) {
    if (min > a[i])
      min = a[i];
    i++;
    getMinElement(a);
  }
  return min;
}
