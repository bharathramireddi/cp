#include <stdio.h>
int add(int a, int b);
int main() {

  int num1, num2, res;
  printf("Enter two numbers : ");
  scanf("%d%d", &num1, &num2);
  res = add(num1, num2);
  printf("\n%d\n", res);
}
int add(int a, int b) {
  while (a > 0) {
    a--;
    b++;
  }
  return b;
}
