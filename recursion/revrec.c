#include <stdio.h>

void revstr();

void main() {
  printf("Please enter a sentence: ");
  revstr();
  printf("\n");
}

int revstr() {
  char c;
  scanf("%c", &c);
  if (c != '\n') {
    revstr();
    printf("%c", c);
  }
}
