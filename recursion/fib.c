#include <stdio.h>
int fib(int n);
int main() {
  int n, i;
  printf("Enter Number ");
  scanf("%d", &n);
  printf("\nFibbonacci --> ");

  for (i = 0; i < n; i++) {
    printf("%d ", fib(i));
  }
}

int fib(int n) {

  if (n > 0) {
    return (fib(n - 1) + fib(n - 2));
  }
}
