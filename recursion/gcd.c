#include <stdio.h>
int gcd(int a, int b);
int main() {
  int num1, num2, res;
  printf("Enter two numbers : ");
  scanf("%d%d", &num1, &num2);
  res = gcd(num1, num2);
  printf("\nGcd --> %d\n", res);
}
int gcd(int a, int b) {
  while (a != b) {
    if (a > b) {
      return gcd(a - b, b);
    } else {
      return gcd(a, b - a);
    }
  }
  return a;
}
