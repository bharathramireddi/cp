#include <stdio.h>

int main() {
  int i, j, m, n;
  int matrix[10][20];
  int sumrow, sumcol;
  printf("Enter number of rows : ");
  scanf("%d", &m);
  printf("Enter number of columns : ");
  scanf("%d", &n);
  for (i = 0; i < m; i++) {
    for (j = 0; j < n; j++) {
      printf("Enter numbers in [%d][%d]: ", i, j);
      scanf("%d", &matrix[i][j]);
    }
  }
  printf("\n");
  for (i = 0; i < m; i++) {
    for (j = 0; j < n; j++) {
      printf("%d\t", matrix[i][j]);
    }
    printf("\n");
  }

  printf("\n");
  for (i = 0; i < m; i++) {
    sumrow = 0;
    for (j = 0; j < n; j++) {
      sumrow = sumrow + matrix[i][j];
    }
    printf("Sum of row %d = %d\n", i + 1, sumrow);
  }

  printf("\n");
  for (i = 0; i < n; i++) {
    sumcol = 0;
    for (j = 0; j < m; j++) {
      sumcol = sumcol + matrix[j][i];
    }
    printf("Sum of column %d = %d\n", i + 1, sumcol);
  }
}
