#include <stdio.h>
int main() {
  int i, j, rows, col, k;
  int a[10][10], b[10][10], mul[10][10];
  printf("Enter rows");
  scanf("%d", &rows);

  printf("enter columns");
  scanf("%d", &col);

  printf("enter elements in A\n");
  for (i = 0; i < rows; i++) {
    for (j = 0; j < col; j++) {
      printf("a %d%d", i + 1, j + 1);
      scanf("%d", &a[i][j]);
    }
  }
  printf("enter elements in B\n");
  for (i = 0; i < rows; i++) {
    for (j = 0; j < col; j++) {
      printf("a %d%d", i + 1, j + 1);
      scanf("%d", &b[i][j]);
    }
  }

  for (i = 0; i < rows; ++i)
    for (j = 0; j < col; ++j) {
      mul[i][j] = 0;
    }

  for (i = 0; i < rows; i++) {
    for (j = 0; j < col; j++) {
      for (k = 0; k < col; k++) {
        mul[i][j] = mul[i][j] + a[i][k] * b[k][j];
      }
    }
  }
  printf("\nOutput Matrix:\n");
  for (i = 0; i < rows; ++i)
    for (j = 0; j < col; ++j) {
      printf("%d  ", mul[i][j]);
      if (j == col - 1)
        printf("\n\n");
    }
  return 0;
}
