#include <stdio.h>
#include <string.h>

int main() {
  int arr[100], i, j = 0, n, search;

  printf("Enter number of elements");
  scanf("%d", &n);
  for (i = 0; i < n; i++) {
    scanf("%d", &arr[i]);
  }
  printf("Enter number to search");
  scanf("%d", &search);
  while (j < n) {
    if (search == arr[j]) {
      printf("Found at %d", j + 1);
    }
    j++;
  }
}
