#include <stdio.h>
union student {
  int percentage;
  int id;
  char name;
};
int main() {
  union student stu1;
  stu1.percentage = 67;
  stu1.id = 10;
  printf("id-->%d\npercentage-->%d", stu1.id, stu1.percentage);
  printf("\nUnion access only one variable at a time\n");
}
