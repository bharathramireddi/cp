#include <stdio.h>
//#pragma pack(1) if we define allocation will be same
struct stu1 {
  int a;
  char b;
  char c;
};
struct stu2 {
  char c;
  int a;
  char b;
};
int main() {
  struct stu1 obj1;
  struct stu2 obj2;
  int res1, res2;
  res1 = sizeof(obj1);
  res2 = sizeof(obj2);
  printf("%d -- stu1 \n%d -- stu2 \nValue difference due to data alignment",
         res1, res2);
}
