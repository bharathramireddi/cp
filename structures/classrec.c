#include <stdio.h>
#include <string.h>

struct student {
  int id;
  char name[20];
  int percent;
};

int main() {
  int i = 0, n;
  struct student record[10];
  printf("Enter no of students ");
  scanf("%d", &n);
  while (i < n) {
    printf("Enter id  ");
    scanf("%d", &record[i].id);
    printf("Enter Percent ");
    scanf("%d", &record[i].percent);
    printf("Enter name  ");
    scanf("%s", &record[i].name);
    i++;
  }
  i = 0;
  while (i < n) {
    printf(" Id : %d \n", record[i].id);
    printf(" Name : %s \n", record[i].name);
    printf(" Percentage : %d \n", record[i].percent);
    i++;
  }
}
