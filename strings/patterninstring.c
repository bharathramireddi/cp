#include <stdio.h>
#include <string.h>
int main() {
  int i = 0, j, found = 0, pos;
  char str[100], sub[20];

  printf("Enter main String  ");
  scanf("%s", &str);
  printf("Enter pattern  ");
  scanf("%s", &sub);
  while (str[i] != NULL) {
    if (str[i] == sub[0]) {
      j = 1;
      while (sub[j] == str[j + i]) {
        j++;
      }
      if (sub[j] == NULL) {
        pos = i;
        found = 1;
      }
    }

    i++;
  }
  if (found == 1)
    printf("---FOUND---at Position %d", pos + 1);
  else
    printf("Not Found");
}
