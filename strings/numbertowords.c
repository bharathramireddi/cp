#include <stdio.h>
int main() {
  int ch, n, num, rem;
  printf("Enter number");
  scanf("%d", &n);
  while (n != 0) {
    rem = n % 10;
    num = rem + num * 10;
    n = n / 10;
  }
  while (num != 0) {
    ch = num % 10;
    switch (ch) {
    case 1:
      printf("one ");
      break;
    case 2:
      printf("two ");
      break;
    case 3:
      printf("Three ");
      break;
    case 4:
      printf("Four ");
      break;
    case 5:
      printf("Five ");
      break;
    case 6:
      printf("Six ");
      break;
    case 7:
      printf("Seven ");
      break;
    case 8:
      printf("Eight ");
      break;
    case 9:
      printf("Nine ");
      break;
    case 0:
      printf("Zero ");
      break;
    }

    num = num / 10;
  }
}
