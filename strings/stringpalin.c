#include <stdio.h>
int main() {
  int i, len, p = 0;
  char a[50];
  printf("Enter string :");
  scanf("%s", a);
  len = strlen(a);
  for (i = 0; i < len; i++) {
    if (a[i] != a[len - 1 - i]) {
      p = 1;
      break;
    }
  }
  if (p == 0) {
    printf("Palindrome");
  } else
    printf("Not Palindrome");
}
