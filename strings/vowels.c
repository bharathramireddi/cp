#include <stdio.h>
#include <string.h>
int main() {
  char a[150];
  int i, len, vow = 0, con = 0, space = 0;
  printf("Enter String: ");
  scanf("%[^\n]s", a);
  len = strlen(a);
  for (i = 0; i < len; i++) {
    if (a[i] == 'a' || a[i] == 'e' || a[i] == 'i' || a[i] == 'o' ||
        a[i] == 'u' || a[i] == 'A' || a[i] == 'E' || a[i] == 'I' ||
        a[i] == 'O' || a[i] == 'U') {
      vow = vow + 1;
    }

    if (a[i] == ' ') {
      space = space + 1;
    } else if (a[i] >= 'a' && a[i] <= 'z') {
      con = con + 1;
    }
  }
  printf("Vowels: %d", vow);
  printf("\nConsonants: %d", con - vow);
  printf("\nWhite spaces: %d", space);
}
