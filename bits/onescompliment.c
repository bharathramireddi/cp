#include <stdio.h>
int main(int argc, char *argv[]) {
  int n = 3, k, i, one[31];
  printf("Enter N value : ");
  scanf("%d", &n);
  for (i = 31; i >= 0; i--) {
    k = n >> i;
    if (k & 1) {
      printf("1");
      one[i] = 0;
    } else {
      printf("0");
      one[i] = 1;
    }
  }
  printf("\nOnes Compliment\n");
  for (i = 31; i >= 0; i--) {
    printf("%d", one[i]);
  }
}
