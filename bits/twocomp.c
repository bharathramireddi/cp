#include <stdio.h>
int main(int argc, char *argv[]) {
  int n = 3, k, i, one[31], two[31], carry = 1;
  printf("Enter N value : ");
  scanf("%d", &n);
  for (i = 31; i >= 0; i--) {
    k = n >> i;
    if (k & 1) {
      printf("1");
      one[i] = 0;
    } else {
      printf("0");
      one[i] = 1;
    }
  }
  printf("\n 1'S Compliment\n");
  for (i = 31; i >= 0; i--) {
    printf("%d", one[i]);
  }
  printf("\n\n2'S Compliment\n");
  for (i = 0; i <= 31; i++) {
    if (one[i] == 1 && carry == 1) {
      two[i] = 0;
    } else if (one[i] == 0 && carry == 1) {
      two[i] = 1;
      carry = 0;
    } else {
      two[i] = one[i];
    }
  }
  for (i = 31; i >= 0; i--) {
    printf("%d", two[i]);
  }
  printf("\n");
}
