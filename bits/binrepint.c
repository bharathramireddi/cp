#include <stdio.h>
int main(int argc, char *argv[]) {
  int i, n, k;
  printf("Enter N value");
  scanf("%d", &n);
  for (i = 31; i >= 0; i--) {
    k = n >> i;
    if (k & 1) {
      printf("1");
    } else {
      printf("0");
    }
  }
}
