#include <stdio.h>
int main(int argc, char *argv[]) {
  int i, n, k, count = 0;
  printf("Enter N value");
  scanf("%d", &n);
  for (i = 31; i >= 0; i--) {
    k = n >> i;
    if (k & 1) {
      count++;
    }
  }
  printf("%d\n", count);
}
