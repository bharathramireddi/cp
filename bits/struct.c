#include <stdio.h>
struct bits {
  unsigned int i;
  unsigned int j;
} bit1;
struct bitfield {
  unsigned int i : 1;
  unsigned int j : 1;
} bit2;
int main() {
  int a, b;
  a = sizeof(bit1);
  b = sizeof(bit2);
  printf("\n1-->%d", a);
  printf("\n2-->%d\n", b);
}
