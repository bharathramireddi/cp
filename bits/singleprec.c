#include <stdio.h>
singleprec(int, double fractional);
binint(int integral, int prec);
binfrac(double fractional);
int main() {
  int i, pos, arr, k, integral;
  double n, fractional;
  printf("Enter N value :");
  scanf("%lf", &n);
  integral = (int)n;
  fractional = n - integral;
  printf("Bit-");
  pos = binint(integral, 31);
  binfrac(fractional);
  printf("\n");
  printf("\nExponent\tMantessa\n");
  singleprec(pos, fractional);
}
binint(int integral, int prec) {
  int i, j, k, a[31], temp[31];
  for (i = prec; i >= 0; i--) {
    k = integral >> i;
    if (k & 1) {
      printf("1");
      a[i] = 1;
    } else {
      printf("0");
      a[i] = 0;
    }
  }
  printf(".");
  for (i = 31; i >= 0; i--) {
    temp[j] = a[i];
    j++;
  }
  int pos, powof2, l;
  for (l = 0; l <= 31; l++) {

    if (temp[l] == 1) {
      pos = 31 - l;
      break;
    }
  }
  return pos;
}

binfrac(double fractional) {
  int l, prec;
  for (l = 0; l < 23; l++) {
    if (fractional != 0) {
      fractional *= 2;
      if (fractional > 1) {
        printf("1");
        fractional = fractional - 1;
      } else {
        printf("0");
      }
    }
  }
  printf("\n");
}
singleprec(int pos, double fractional) {
  int E = 127, S;
  S = E + pos;
  binint(S, 7);
  printf(" ");
  binfrac(fractional);
}
